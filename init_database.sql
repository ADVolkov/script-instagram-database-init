BEGIN;
CREATE TABLE Feedbacks
(
    id      INTEGER PRIMARY KEY,
    name    VARCHAR(100) NOT NULL,
    email   VARCHAR(100) NOT NULL,
    message VARCHAR(500) NOT NULL
);
CREATE TABLE UploadFiles
(
    id   INTEGER PRIMARY KEY,
    name VARCHAR,
    data BYTEA NOT NULL
);
CREATE TABLE Users
(
    id              INTEGER PRIMARY KEY,
    username        VARCHAR(50)  NOT NULL UNIQUE,
    email           VARCHAR(100) NOT NULL UNIQUE,
    hashed_password VARCHAR      NOT NULL,
    avatar_file_id  INT UNIQUE,
    balance         numeric      NOT NULL DEFAULT 0,
    info            VARCHAR(1000),
    rate            NUMERIC(3, 2),
    CHECK (rate <= 5 and rate > 0),
    FOREIGN KEY (avatar_file_id) REFERENCES UploadFiles (id) ON DELETE SET NULL
);
CREATE TABLE SpecializationTypes
(
    id   INTEGER PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE
);
CREATE TABLE OrderSpecializations
(
    id      INTEGER PRIMARY KEY,
    name    VARCHAR(50) NOT NULL UNIQUE,
    type_id INT         NOT NULL,
    FOREIGN KEY (type_id) REFERENCES SpecializationTypes (id) ON DELETE SET NULL
);
CREATE TABLE OrderTypes
(
    id   INTEGER PRIMARY KEY,
    name VARCHAR(50) NOT NULL UNIQUE
);
CREATE TABLE Reviews
(
    id    INTEGER PRIMARY KEY,
    text  VARCHAR(300),
    grade INT NOT NULL,
    CONSTRAINT correct_grade CHECK (grade in (1, 2, 3, 4, 5))
);
CREATE TABLE Orders
(
    id                 INTEGER PRIMARY KEY,
    name               VARCHAR(50) NOT NULL,
    specialization_id  INT         NOT NULL,
    type_id            INT         NOT NULL,
    additional_file_id INT UNIQUE,
    solution_file_id   INT UNIQUE,
    description        VARCHAR(300),
    deadline           DATE        NOT NULL,
    status             VARCHAR(20) NOT NULL,
    price              INT         NOT NULL,
    customer_id        INT         NOT NULL,
    executor_id        INT,
    review_id          INT UNIQUE,
    CONSTRAINT correct_price CHECK (price > 0),
    FOREIGN KEY (review_id) REFERENCES Reviews (id),
    FOREIGN KEY (specialization_id) REFERENCES OrderSpecializations (id),
    FOREIGN KEY (type_id) REFERENCES OrderTypes (id),
    FOREIGN KEY (additional_file_id) REFERENCES UploadFiles (id),
    FOREIGN KEY (solution_file_id) REFERENCES UploadFiles (id),
    FOREIGN KEY (customer_id) REFERENCES Users (id),
    FOREIGN KEY (executor_id) REFERENCES Users (id)
);
CREATE TABLE ChatMessages
(
    id          INTEGER PRIMARY KEY,
    user_id     INT  NOT NULL,
    order_id    INT  NOT NULL,
    message     TEXT NOT NULL,
    public_date DATE NOT NULL,
    FOREIGN KEY (user_id) REFERENCES Users (id),
    FOREIGN KEY (order_id) REFERENCES Orders (id)
);
INSERT INTO feedbacks (name, email, message)
VALUES ('Rustem', 'rustem@yandex.ru', 'Вы можете мне помочь?Ну помогите');
INSERT INTO users (id, username, email, hashed_password, info, balance)
VALUES (1, 'nagibator228', 'test1@mail.ru',
        'pbkdf2:sha256:260000$AdgaQwmjKCBhdRjE$c878178a16cab0d6fafaf1294e0924ee73f290b94ee2babab1a9ccc64b7f6cbc',
        'Решу все ваши проблемы за хорошие деньги', 100);
INSERT INTO users (id, username, email, hashed_password, info, balance)
VALUES (2, 'sanya322', 'test2@mail.ru',
        'pbkdf2:sha256:260000$AdgaQwmjKCBhdRjE$c878178a16cab0d6fafaf1294e0924ee73f290b94ee2babab1a9ccc64b7f6cbc',
        'Красиво сделаю', 300);
INSERT INTO users (id, username, email, hashed_password, info, balance)
VALUES (3, 'sexterminator3', 'test3@mail.ru',
        'pbkdf2:sha256:260000$AdgaQwmjKCBhdRjE$c878178a16cab0d6fafaf1294e0924ee73f290b94ee2babab1a9ccc64b7f6cbc',
        'Работаю уже более 3 лет, выполняю работы любой сложности', 10);
INSERT INTO users (id, username, email, hashed_password)
VALUES (4, 'm4s', 'test4@mail.ru',
        'pbkdf2:sha256:260000$AdgaQwmjKCBhdRjE$c878178a16cab0d6fafaf1294e0924ee73f290b94ee2babab1a9ccc64b7f6cbc');
INSERT INTO SpecializationTypes (id, name)
VALUES (1, 'Программирование');
INSERT INTO SpecializationTypes (id, name)
VALUES (2, 'Математика');
INSERT INTO SpecializationTypes (id, name)
VALUES (3, 'Физика');
INSERT INTO SpecializationTypes (id, name)
VALUES (4, 'Иностранный язык');
INSERT INTO orderspecializations (id, name, type_id)
VALUES (1, 'Python', 1);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (2, 'C++', 1);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (3, 'Java', 1);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (4, 'Высшая математика', 2);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (5, 'Алгебра', 2);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (6, 'Геометрия', 2);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (7, 'Квантовая физика', 3);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (8, 'Ядерная физика', 3);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (9, 'Английский язык', 4);
INSERT INTO orderspecializations (id, name, type_id)
VALUES (10, 'Немецкий язык', 4);
INSERT INTO ordertypes (id, name)
VALUES (1, 'Семестровая работа');
INSERT INTO ordertypes (id, name)
VALUES (2, 'Контрольная работа');
INSERT INTO ordertypes (id, name)
VALUES (3, 'Доклад');
INSERT INTO ordertypes (id, name)
VALUES (4, 'Презентация');
INSERT INTO ordertypes (id, name)
VALUES (5, 'Тест');
INSERT INTO orders (name, specialization_id, type_id, deadline, status, price,
                    customer_id)
VALUES ('Сделеать семестровую работу Flask', 1, 1, '2021-10-26'::DATE, 'Опубликовано', 20000, 1);
INSERT INTO orders (name, specialization_id, type_id, deadline, status, price,
                    customer_id)
VALUES ('Сделеать семестровую работу Django', 1, 1, '2021-10-26'::DATE, 'Опубликовано', 20000, 2);
INSERT INTO orders (name, specialization_id, type_id, deadline, status, price,
                    customer_id)
VALUES ('Тест по теории вероятностей', 4, 5, '2021-10-26'::DATE, 'Опубликовано', 20000, 3);
INSERT INTO orders (name, specialization_id, type_id, deadline, status, price,
                    customer_id)
VALUES ('Контрольная по английскому, срочно', 9, 2, '2021-10-26'::DATE, 'Опубликовано', 20000, 4);
COMMIT;
