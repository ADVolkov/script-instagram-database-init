-- 1) поменять отношение posts.file_id - upload_file с one-to-one на one-to-many, те чтобы в одном посте могло быть несколько фотографий
-- 2) добавить в таблицу User булевое поле - верифицирован ли (имеет ли галочку)(по дефолту false)
-- 3) изменить в таблице Users длину поля info с 1000 на 228 символов и переименовать на information

BEGIN TRANSACTION;
SET LOCAL lock_timeout TO '100ms';
CREATE TABLE PostFiles
(
    post_id INTEGER,
    file_id INTEGER UNIQUE,
    CONSTRAINT post_id_file_id_pk PRIMARY KEY (post_id, file_id),
    CONSTRAINT post_id_fk FOREIGN KEY (post_id) REFERENCES posts (id),
    CONSTRAINT file_id_fk FOREIGN KEY (file_id) REFERENCES uploadfiles (id)
);
ALTER TABLE posts ALTER COLUMN file_id DROP NOT NULL;
ALTER TABLE posts DROP COLUMN file_id CASCADE;
ALTER TABLE users ADD COLUMN is_verified BOOLEAN DEFAULT FALSE;
ALTER TABLE users DROP COLUMN info CASCADE;
ALTER TABLE users ADD COLUMN information VARCHAR(228);
COMMIT;
