BEGIN TRANSACTION;
SET LOCAL lock_timeout TO '100ms';
ALTER TABLE users
    DROP COLUMN information CASCADE;
ALTER TABLE users
    ADD COLUMN info VARCHAR(1000);
ALTER TABLE users
    ALTER COLUMN is_verified DROP DEFAULT;
ALTER TABLE users
    DROP COLUMN is_verified CASCADE;
ALTER TABLE posts
    ADD COLUMN file_id INT;
ALTER TABLE posts
    ADD CONSTRAINT post_file_unique UNIQUE (file_id);
ALTER TABLE posts
    ADD CONSTRAINT file_not_null
        CHECK (posts.file_id IS NOT NULL) NOT VALID;
ALTER TABLE posts ADD FOREIGN KEY (file_id)
  REFERENCES uploadfiles (id);
ALTER TABLE postfiles DROP CONSTRAINT post_id_file_id_pk;
ALTER TABLE postfiles DROP COLUMN post_id RESTRICT;
ALTER TABLE postfiles DROP COLUMN file_id CASCADE;
DROP TABLE postfiles;
COMMIT;
