CREATE INDEX idx_location_city ON Locations USING HASH (city); -- нужен, чтобы при вводе города (когда человек добавляет локацию к посту) получать сразу и соответствующую страну, выбрал hash тк города уникальны
CREATE INDEX idx_username ON Users USING HASH (username); -- чтобы можно было быстро найти пользователя, выбрал hash тк пользователи уникальны
CREATE INDEX idx_post_owner ON Posts (owner_id); -- чтобы можно было, зайдя на страницу человеа, быстро получить все его посты, выбрал b-tree тк у пользователя может быть много постов(owner не уникальный)
CREATE INDEX idx_post_likes ON Likes (post_id); -- чтобы быстро найти все лайки у поста, b-tree тк лайков может быть много
CREATE INDEX idx_subscribers ON Subscriptions (subscriber_user_id); -- чтобы быстро находить всех подписчиков, b-tree тк значения не уникальны
CREATE INDEX idx_subscriptions ON Subscriptions (tracked_user_id); -- чтобы быстро находить все подписки, b-tree тк значения не уникальны
CREATE INDEX idx_stories_viewer ON StoriesViews (stories_id);
-- чтобы у истории быстро найти все просмотры, b-tree тк просмотров может быть много

-- при добавлении локации к посту вбиваем в поиске город и получаем полную локацию
SELECT city,
       country
FROM locations
WHERE city = 'Outlook';

-- отображение страницы пользователя с его профилем с фотограифями с постов
SELECT username,
       (SELECT COUNT(*) FROM subscriptions WHERE tracked_user_id = users.id)    as subscribers_count,
       (SELECT COUNT(*) FROM subscriptions WHERE subscriber_user_id = users.id) as subscriptions_count,
       info,
       avatar.data,
       photos.data
FROM users
         left join posts on users.id = posts.owner_id
         left join uploadfiles photos on posts.file_id = photos.id
         left join uploadfiles avatar on users.avatar_file_id = avatar.id
where username = 'mcleverly2'
order by public_date desc;

-- получение историй пользователя
SELECT username,
       avatar.data,
       st.data,
       public_date
FROM users
         LEFT JOIN stories ON stories.owner_id = users.id
         LEFT JOIN uploadfiles avatar ON users.avatar_file_id = avatar.id
         LEFT JOIN uploadfiles st ON st.id = stories.file_id
WHERE username = 'abeddow6'
order by public_date;

-- просмотр пользователей посмотревших историю
SELECT viewer.username,
       viewer_avatar.data
FROM storiesviews
         LEFT JOIN stories s on s.id = storiesviews.stories_id
         LEFT JOIN users viewer on viewer.id = viewer_user_id
         LEFT JOIN uploadfiles viewer_avatar on viewer_avatar.id = viewer.avatar_file_id
where storiesviews.stories_id = 47;

-- просмотр поста
SELECT country,
       city,
       image.data,
       c.public_date,
       writer.username,
       writer_avatar.data,
       c.content,
       (SELECT COUNT(*) as post_count_likes
        FROM likes
        WHERE post_id = posts.id)
FROM posts
         left join locations on locations.id = posts.location_id
         left join uploadfiles image on image.id = posts.file_id
         left join comments c on posts.id = c.post_id
         left join users writer on writer.id = c.writer_id
         left join uploadfiles writer_avatar on writer_avatar.id = writer.avatar_file_id
where posts.id = 2
order by public_date;

-- получение подписок полльзователя
SELECT username,
       data
FROM subscriptions
         left join users u on subscriptions.tracked_user_id = u.id
         left join uploadfiles av on u.avatar_file_id = av.id
WHERE subscriber_user_id = 2;

-- получение подписчков полльзователя
SELECT username,
       data
FROM subscriptions
         left join users u on subscriptions.subscriber_user_id = u.id
         left join uploadfiles av on u.avatar_file_id = av.id
WHERE tracked_user_id = 2;

-- запросы с использованием views

SELECT country,
       city,
       image_data,
       c.public_date,
       username,
       user_avatar_data,
       c.content,
       (SELECT COUNT(*) as post_count_likes
        FROM likes
        WHERE post_id = posts_with_image_and_author.post_id)
FROM posts_with_image_and_author
         left join locations on locations.id = posts_with_image_and_author.location_id
         left join comments c on posts_with_image_and_author.post_id = c.post_id
where posts_with_image_and_author.post_id = 2
order by public_date;

SELECT username,
       users_with_avatars.data AS avatar_data,
       stories_with_file.data,
       public_date
FROM users_with_avatars
         LEFT JOIN stories_with_file ON stories_with_file.owner_id = users_with_avatars.id
WHERE username = 'abeddow6'
order by public_date;

-- для materialized views

-- больше ли у пользователя отношение постов с геолокацией чем у всех юзеров
SELECT SUM(CASE WHEN location_id IS NOT NULL THEN 1 ELSE 0 END)::real / COUNT(*) >
       (SELECT SUM(value) FROM proportion_of_posts_with_geolocation_all_users)
FROM posts
WHERE owner_id = 16;

-- больше ли у пользователя постов, чем в среднем у других
SELECT COUNT(*) > (SELECT SUM(value) FROM avg_number_of_posts_per_user)
FROM posts
WHERE owner_id = 4;
